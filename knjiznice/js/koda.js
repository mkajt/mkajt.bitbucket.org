
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

function generirajPodatke() {
  var ehrId = "";
  for(var j = 1; j <= 3; j++){
    if(j == 1){
      $.ajax({
          url: baseUrl + "/ehr",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          success: function (podatki) {
            var ehrId = podatki.ehrId;
            document.getElementById("pacient1").value = ehrId;
            var partyPodatki = {
              firstNames: "Janez", //diabetes tipa 1
              lastNames: "Gorenc",
              dateOfBirth: "1000-01-01",
              additionalInfo: {"ehrId": ehrId}
            };
            $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              headers: {
                "Authorization": getAuthorization()
              },
              contentType: 'application/json',
              data: JSON.stringify(partyPodatki),
              success: function(podatki2) {
                var pod = {
            	    "ctx/language": "en",
            	    "ctx/territory": "SI",
            	    "ctx/time":"1060-11-13T12:45Z" ,
            	    "vital_signs/height_length/any_event/body_height_length":"170",
            	    "vital_signs/body_weight/any_event/body_weight": "58",
            	   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36.00",
            	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            	    "vital_signs/blood_pressure/any_event/systolic": "150",
            	    "vital_signs/blood_pressure/any_event/diastolic": "95",
            	    "vital_signs/indirect_oximetry:0/spo2|numerator": "11.00"
            		};
            		var parametri = {
            		    ehrId: ehrId,
            		    templateId: 'Vital Signs',
            		    format: 'FLAT',
            		    committer: ""
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(parametri),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(pod),
                  headers: {
                    "Authorization": getAuthorization()
                  }
            		});
              }
            });
          }  
      });
      
    }else if(j == 2){
      $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        success: function (podatki) {
          var ehrId = podatki.ehrId;
          document.getElementById("pacient2").value = ehrId;
          var partyPodatki = {
            firstNames: "Micka", //bronhitis
            lastNames: "Dolenc",
            dateOfBirth: "2000-02-02",
            additionalInfo: {"ehrId": ehrId}
          };
          $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            headers: {
              "Authorization": getAuthorization()
            },
            contentType: 'application/json',
            data: JSON.stringify(partyPodatki),
            success: function(podatki2) {
              var pod = {
                "ctx/language": "en",
                "ctx/territory": "SI",
                "ctx/time":"2050-12-23T10:35Z" ,
                "vital_signs/height_length/any_event/body_height_length":"165" ,
                "vital_signs/body_weight/any_event/body_weight": "70",
               	"vital_signs/body_temperature/any_event/temperature|magnitude":"38.40",
                "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                "vital_signs/blood_pressure/any_event/systolic": "110",
                "vital_signs/blood_pressure/any_event/diastolic": "78",
                "vital_signs/indirect_oximetry:0/spo2|numerator": "9.00"
              };
              var parametri = {
                  ehrId: ehrId,
                  templateId: 'Vital Signs',
                  format: 'FLAT',
                  committer: ""
              };
              $.ajax({
                url: baseUrl + "/composition?" + $.param(parametri),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(pod),
                headers: {
                  "Authorization": getAuthorization()
                }
              });
            }
          });
        }  
      });
    	
    }else if(j == 3){
      $.ajax({
          url: baseUrl + "/ehr",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          success: function (podatki) {
            var ehrId = podatki.ehrId;
            document.getElementById("pacient3").value = ehrId;
            var partyPodatki = {
              firstNames: "Lojze", //sportnik
              lastNames: "Stajerc",
              dateOfBirth: "3000-03-03",
              additionalInfo: {"ehrId": ehrId}
            };
            $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              headers: {
                "Authorization": getAuthorization()
              },
              contentType: 'application/json',
              data: JSON.stringify(partyPodatki),
              success: function(podatki2) {
                var pod = {
            	    "ctx/language": "en",
            	    "ctx/territory": "SI",
            	    "ctx/time":"3150-12-14T10:55Z" ,
            	    "vital_signs/height_length/any_event/body_height_length":"180" ,
            	    "vital_signs/body_weight/any_event/body_weight": "78",
            	   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36.60",
            	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            	    "vital_signs/blood_pressure/any_event/systolic": "120",
            	    "vital_signs/blood_pressure/any_event/diastolic": "80",
            	    "vital_signs/indirect_oximetry:0/spo2|numerator": "13.00"
            		};
            		var parametri = {
            		    ehrId: ehrId,
            		    templateId: 'Vital Signs',
            		    format: 'FLAT',
            		    committer: ""
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(parametri),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(pod),
                  headers: {
                    "Authorization": getAuthorization()
                  },
                });
              }
            });
          }  
      });
    }
  }
  return ehrId;
}
window.addEventListener("load", function(){
  if(window.location.pathname != "/index.html" && window.location.pathname != "/mkajt.bitbucket.org/index.html") return;
  var ehrIzbranega = "";
  
  document.getElementById("EhrID").addEventListener("keyup", function(event){
    if(event.keyCode == 13){
      ehrIzbranega = document.getElementById("EhrID").value;
      document.getElementById("potrdi").disabled = false;
    }
  });
  document.getElementById("potrdi").addEventListener("click",function(){
    if(ehrIzbranega == ""){
      var h = document.getElementById("select");
      ehrIzbranega = h.options[h.selectedIndex].value;
    }
    pridobiPodatke(ehrIzbranega);
  });
});
var generirajPod = function(){
    generirajPodatke();
    document.getElementById("select").disabled = false;
    document.getElementById("izberitePacienta").disabled = false;
    document.getElementById("potrdi").disabled = false;
};
var prikaziPodatke = function(podatki) {
    document.getElementById("tez").value = podatki.teza;
    document.getElementById("temp").value = podatki.temperatura;
    document.getElementById("nasic").value = podatki.nasicenost;
    document.getElementById("sist").value = podatki.pritisk2;
    document.getElementById("dist").value = podatki.pritisk1;
    document.getElementById("vis").value = podatki.visina;
    
    var delez = podatki.visina / 100;
    var indeksTM = podatki.teza / (delez * delez);
    var ITM = indeksTM.toFixed(2);
    var vrednost;
    if(ITM < 18.50){
      vrednost = "nedohranjenost";
    }else if(ITM >= 30.00){
      vrednost = "debelost";
    }else if(ITM >= 25.00){
      vrednost = "povišana telesna teža";
    }else{
      vrednost = "normalna telesna teža";
    }
    document.getElementById("itm").value = ITM;
    
    document.getElementById("prikazPodatkov").innerHTML = "<h5>Vaši podatki so:</h5>"
    document.getElementById("prikazPodatkov").innerHTML += "teža: " + podatki.teza + "kg";
    document.getElementById("prikazPodatkov").innerHTML += "<br>višina: " + podatki.visina + "cm";
    document.getElementById("prikazPodatkov").innerHTML += "<br>temperatura: " + podatki.temperatura + "°C";
    document.getElementById("prikazPodatkov").innerHTML += "<br>sistolični tlak: " + podatki.pritisk2 + "mmHg";
    document.getElementById("prikazPodatkov").innerHTML += "<br>diastolični tlak: " + podatki.pritisk1 + "mmHg";
    document.getElementById("prikazPodatkov").innerHTML += "<br>nasičenost krvi s kisikom: " + podatki.nasicenost + "kPa";
    document.getElementById("prikazPodatkov").innerHTML += "<br><h4>Vaš indeks telesne mase je: </h4>" + ITM + " - " + vrednost;
};
var pridobiPodatke = function(ehrId) {
    var podatkiPacienta = {
      nasicenost:0,
      pritisk1:0,
      pritisk2:0,
      teza: 0,
      temperatura: 0,
      visina: 0
    };
    
    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    	type: 'GET',
    	headers: {
      "Authorization": getAuthorization()
    	},
      success: function(podatki) {
        podatkiPacienta.temperatura = podatki[0].temperature;
        $.ajax({
          url: baseUrl + "/view/" + ehrId + "/" + "weight",
        	type: 'GET',
        	headers: {
           "Authorization": getAuthorization()
        	},
        	success: function(podatki) {
            podatkiPacienta.teza = podatki[0].weight;
        	   $.ajax({
              url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
        	    type: 'GET',
              headers: {
                "Authorization": getAuthorization()
        	    },
        	    success: function(podatki) {
        	      podatkiPacienta.pritisk2 = podatki[0].systolic;
        	      $.ajax({
                  url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
        	        type: 'GET',
                  headers: {
                    "Authorization": getAuthorization()
                  },
                  success: function(podatki) {
        	          podatkiPacienta.pritisk1 = podatki[0].diastolic;
        	          $.ajax({
                      url: baseUrl + "/view/" + ehrId + "/" + "spO2",
        	            type: 'GET',
                      headers: {
                      "Authorization": getAuthorization()
                      },
                      success: function(podatki){
                        podatkiPacienta.nasicenost = podatki[0].spO2;
                        $.ajax({
                          url: baseUrl + "/view/" + ehrId + "/" + "height",
        	                type: 'GET',
                          headers: {
                          "Authorization": getAuthorization()
                          },
                          success: function(podatki){
                          podatkiPacienta.visina = podatki[0].height;
                          prikaziPodatke(podatkiPacienta);
                        }  
                      });
                      }
                    });
                  }
        	      });
        	     }
        	   }); 
        	}
        });
      }
    });
  };
var generiraj = function(seznam) {
	var konec = "";
  for (var i = 0; i < seznam.length; i++) {
  	konec += "<option>" + seznam[i] + "</option>";
  }
  return konec;
};